# Copyright (C) 2019 O.S. Autogramma LLC.

DESCRIPTION = "User application files for SDU"
LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=39ec502560ab2755c4555ee8416dfe42"

RDEPENDS_${PN} = "busybox"

INSANE_SKIP_${PN} += "already-stripped"

SRC_URI = "file://LICENSE \
           file://bin/tracker-default-version/all-services \
           file://bin/tracker-default-version/dhcp-wlan0 \
           file://bin/tracker-default-version/params.json \
           file://bin/tracker-default-version/ssh-tunnel \
           file://bin/tracker-default-version/tracker \
           file://bin/tracker-default-version/tracker-main \
           file://bin/tracker-default-version/tracker-packer \
           file://bin/tracker-default-version/tracker-uploader \
           file://bin/tracker-default-version/tracker-updater \
           file://bin/sdu_restart \
           file://etc/tracker-default-cfg/syslog-startup.conf \
           file://etc/tracker-default-cfg/wpa_supplicant.conf \
           file://bin/sdu_log_mess \
           "

S = "${WORKDIR}"

do_install () {
    install -d ${D}/bin
    install -d ${D}/bin/tracker-default-version
    install -m 755 ${S}/bin/tracker-default-version/all-services ${D}/bin/tracker-default-version/all-services
    install -m 755 ${S}/bin/tracker-default-version/dhcp-wlan0 ${D}/bin/tracker-default-version/dhcp-wlan0
    install -m 755 ${S}/bin/tracker-default-version/params.json ${D}/bin/tracker-default-version/params.json
    install -m 755 ${S}/bin/tracker-default-version/ssh-tunnel ${D}/bin/tracker-default-version/ssh-tunnel
    install -m 755 ${S}/bin/tracker-default-version/tracker ${D}/bin/tracker-default-version/tracker
    install -m 755 ${S}/bin/tracker-default-version/tracker-main ${D}/bin/tracker-default-version/tracker-main
    install -m 755 ${S}/bin/tracker-default-version/tracker-packer ${D}/bin/tracker-default-version/tracker-packer
    install -m 755 ${S}/bin/tracker-default-version/tracker-uploader ${D}/bin/tracker-default-version/tracker-uploader
    install -m 755 ${S}/bin/tracker-default-version/tracker-updater ${D}/bin/tracker-default-version/tracker-updater
    install -m 755 ${S}/bin/sdu_restart ${D}/bin/sdu_restart
    install -m 755 ${S}/bin/sdu_log_mess ${D}/bin/sdu_log_mess
    install -d ${D}/etc
    install -d ${D}/etc/tracker-default-cfg
    install -m 755 ${S}/etc/tracker-default-cfg/syslog-startup.conf ${D}/etc/tracker-default-cfg/syslog-startup.conf
    install -m 755 ${S}/etc/tracker-default-cfg/wpa_supplicant.conf ${D}/etc/tracker-default-cfg/wpa_supplicant.conf
}

#syslog и interfaces поправленный в другом репозитории через .bbapend
